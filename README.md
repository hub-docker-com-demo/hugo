# [hugo](https://hub-docker-com-demo.gitlab.io/hugo)

Fast and flexible Static Site Generator https://gohugo.io

![Debian popcon image](https://qa.debian.org/cgi-bin/popcon-png?packages=hugo%20jekyll%20mkdocs%20pelican%20nanoc%20gitit%20chronicle&show_installed=on&want_percent=1&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)

# Books
*  Hugo in Action: Static sites and dynamic Jamstack apps 2022 Atishay Jain

# Official Documentation
* [gohugo.io](https://gohugo.io)
  * [*Quick Start*
    ](https://gohugo.io/getting-started/quick-start/)
  * [*Host on GitLab*
    ](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/)
    * https://gitlab.com/pages/hugo
      * image: registry.gitlab.com/pages/hugo:latest
* [Manpages of hugo in Debian testing
  ](https://manpages.debian.org/testing/hugo/)
  * https://gohugo.io/commands

# Static website compilers
* https://gitlab.com/javascript-packages-demo/staticsitegenerators

# Repology
* https://repology.org/project/hugo-sitegen
